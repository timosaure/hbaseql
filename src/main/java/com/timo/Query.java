package com.timo;

import groovy.lang.Closure;

public class Query {
	
	public static Query make(Closure<?> closure) {
		Query query = new Query();
		closure.setDelegate(query);
		closure.call();
		return query;
	}

	private String what;
	private String source;
	private String column;
	private String value;
	
	public void select(String what) {
		this.what = what;
	}

	public void from(String source) {
		this.source = source;
	}
	
	public void where(String column) {
		this.column = column;
	}
	
	public void equal(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return String.format("Selects column %s from table %s where the column %s equals %s", what, source, column, value);
	}
}
