package com.timo;

import groovy.lang.Script;

public abstract class QueryScript extends Script{
	
	private String what;
	private String source;
	private String column;
	private String value;
	
	public QueryScript select(String what) {
		this.what = what;
		return this;
	}

	public QueryScript from(String source) {
		this.source = source;
		return this;
	}
	
	public QueryScript where(String column) {
		this.column = column;
		return this;
	}
	
	public QueryScript equal(String value) {
		this.value = value;
		return this;
	}
	
	public void printQuery() {
		System.out.println(String.format("Selects column %s from table %s where the column %s equals %s", what, source, column, value));
	}

}
